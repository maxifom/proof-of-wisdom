package client

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"time"

	"go.uber.org/zap"

	"proof-of-wisdom/internal/pow"
	"proof-of-wisdom/internal/protocol"
)

// Run - dials client to server and work on receiving quotes from server
// Client attempts to solve PoW challenge and receive quote with interval of 5 seconds.
func Run(ctx context.Context, address string, logger *zap.Logger, maxIterations int) error {
	d := net.Dialer{Timeout: 5 * time.Second}
	conn, err := d.DialContext(ctx, "tcp", address)
	if err != nil {
		return fmt.Errorf("failed to dial %s: %w", address, err)
	}
	defer conn.Close()

	for {
		done := make(chan error)
		go func() {
			connCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
			defer cancel()
			quote, err := handleConnection(connCtx, conn, logger, maxIterations)
			if err != nil {
				done <- err

				return
			}
			logger.Info("Received quote", zap.String("quote", quote))
			done <- nil
		}()

		select {
		case err = <-done:
			if err != nil {
				return err
			}
		case <-ctx.Done():
			return ctx.Err()
		}

		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-time.After(5 * time.Second):
			continue
		}
	}
}

type handleResult struct {
	quote string
	err   error
}

func handleConnection(ctx context.Context, conn io.ReadWriter, logger *zap.Logger, maxIterations int) (string, error) {
	reader := bufio.NewReader(conn)

	done := make(chan handleResult)
	go func() {
		hashcash, err := requestChallenge(conn, reader, logger)
		if err != nil {
			done <- handleResult{err: err}

			return
		}

		hashcash, err = computeHashcash(hashcash, maxIterations, logger)
		if err != nil {
			done <- handleResult{err: err}

			return
		}

		err = sendHashcashSolution(conn, hashcash, logger)
		if err != nil {
			done <- handleResult{err: err}

			return
		}

		quote, err := getQuote(reader)
		done <- handleResult{quote: quote, err: err}
	}()

	select {
	case <-ctx.Done():
		return "", ctx.Err()
	case res := <-done:
		return res.quote, res.err
	}
}

func requestChallenge(conn io.ReadWriter, reader *bufio.Reader, logger *zap.Logger) (pow.HashcashData, error) {
	requestChallengeMessage := protocol.Message{Header: protocol.RequestChallenge}
	err := protocol.WriteMessage(conn, requestChallengeMessage)
	if err != nil {
		return pow.HashcashData{}, fmt.Errorf("failed to send request challenge message: %w", err)
	}

	quoteMsg, err := protocol.ReadMessage(reader)
	if err != nil {
		return pow.HashcashData{}, fmt.Errorf("failed to read request challenge response: %w", err)
	}

	var hashcash pow.HashcashData
	err = json.Unmarshal(quoteMsg.Payload, &hashcash)
	if err != nil {
		return pow.HashcashData{}, fmt.Errorf("failed to unmarshal hashcash challenge: %w", err)
	}

	logger.Debug("Received hashcash", zap.Any("hashcash_data", hashcash))

	return hashcash, nil
}

func computeHashcash(hashcash pow.HashcashData, maxIterations int, logger *zap.Logger) (pow.HashcashData, error) {
	hashcash, err := hashcash.ComputeHashcash(maxIterations)
	if err != nil {
		return hashcash, fmt.Errorf("failed to compute hashcash: %w", err)
	}

	logger.Debug("Hashcash computed result", zap.Any("hashcash", hashcash))

	return hashcash, nil
}

func sendHashcashSolution(conn io.ReadWriter, hashcash pow.HashcashData, logger *zap.Logger) error {
	hashcashJSON, err := json.Marshal(hashcash)
	if err != nil {
		return fmt.Errorf("failed to marshal hashcash: %w", err)
	}
	requestResourceMessage := protocol.Message{
		Header:  protocol.RequestResource,
		Payload: hashcashJSON,
	}
	err = protocol.WriteMessage(conn, requestResourceMessage)
	if err != nil {
		return fmt.Errorf("failed to send request resource message: %w", err)
	}

	logger.Debug("Challenge sent to server")

	return nil
}

func getQuote(reader *bufio.Reader) (string, error) {
	quoteMsg, err := protocol.ReadMessage(reader)
	if err != nil {
		return "", fmt.Errorf("failed to read quote message from server: %w", err)
	}

	return string(quoteMsg.Payload), nil
}
