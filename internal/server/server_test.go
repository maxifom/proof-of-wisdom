package server

import (
	"encoding/base64"
	"encoding/json"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"

	"proof-of-wisdom/internal/pow"
	"proof-of-wisdom/internal/protocol"
)

// MockClock - mock for Clock interface (to work with predefined Now).
type MockClock struct {
	NowFunc func() time.Time
}

func (m *MockClock) Now() time.Time {
	if m.NowFunc != nil {
		return m.NowFunc()
	}

	return time.Now()
}
func TestProcessRequest(t *testing.T) {
	t.Parallel()

	cfg := HashcashConfig{ZerosCount: 3, Duration: 300 * time.Millisecond}
	const randKey = 123460

	tests := []struct {
		name        string
		input       *protocol.Message
		client      string
		nowFunc     func() time.Time
		expectedErr string
		expectedMsg *protocol.Message
		validate    func(t *testing.T, msg *protocol.Message)
	}{
		{
			name:   "Request challenge",
			input:  &protocol.Message{Header: protocol.RequestChallenge},
			client: "client1",
			validate: func(t *testing.T, msg *protocol.Message) {
				t.Helper()
				require.NotNil(t, msg)
				assert.Equal(t, protocol.ResponseChallenge, msg.Header)
				var hashcash pow.HashcashData
				err := json.Unmarshal(msg.Payload, &hashcash)
				require.NoError(t, err)
				assert.Equal(t, 3, hashcash.ZerosCount)
				assert.Equal(t, "client1", hashcash.Resource)
				assert.NotEmpty(t, hashcash.Rand)
			},
		},
		{
			name:        "Request resource without solution",
			input:       &protocol.Message{Header: protocol.RequestResource},
			client:      "client1",
			expectedErr: "failed to unmarshal hashcash: unexpected end of JSON input",
		},
		{
			name:   "Request resource with wrong resource",
			client: "client1",
			input: func() *protocol.Message {
				hashcash := pow.HashcashData{
					Version:    1,
					ZerosCount: 4,
					Date:       time.Now().Unix(),
					Resource:   "client2",
					Rand:       base64.StdEncoding.EncodeToString([]byte(strconv.FormatInt(randKey, 10))),
					Counter:    100,
				}
				marshaled, _ := json.Marshal(hashcash)
				return &protocol.Message{Header: protocol.RequestResource, Payload: marshaled}
			}(),
			expectedErr: "invalid hashcash resource client2 != client1",
		},
		{
			name:   "Request resource with expired solution",
			client: "client1",
			nowFunc: func() time.Time {
				return time.Date(2022, 3, 13, 2, 40, 0, 0, time.UTC)
			},
			input: func() *protocol.Message {
				hashcash := pow.HashcashData{
					Version:    1,
					ZerosCount: 10,
					Date:       time.Date(2022, 3, 13, 2, 30, 0, 0, time.UTC).Unix(),
					Resource:   "client1",
					Rand:       base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(randKey))),
					Counter:    100,
				}
				marshaled, _ := json.Marshal(hashcash)
				return &protocol.Message{Header: protocol.RequestResource, Payload: marshaled}
			}(),
			expectedErr: "challenge expired 10m0s > 300ms",
		},
		{
			name:   "Request resource with correct solution",
			client: "client1",
			nowFunc: func() time.Time {
				return time.Date(2018, 4, 12, 5, 21, 0, 0, time.UTC)
			},
			input: func() *protocol.Message {
				date := time.Date(2018, 4, 12, 5, 22, 0, 0, time.UTC)
				hashcash := pow.HashcashData{
					Version:    1,
					ZerosCount: 2,
					Date:       date.Unix(),
					Resource:   "client1",
					Rand:       base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(randKey))),
					Counter:    9921,
				}
				marshaled, _ := json.Marshal(hashcash)
				return &protocol.Message{Header: protocol.RequestResource, Payload: marshaled}
			}(),
			validate: func(t *testing.T, msg *protocol.Message) {
				t.Helper()
				require.NotNil(t, msg)
				assert.Contains(t, Quotes, string(msg.Payload))
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			clock := MockClock{}
			if tt.nowFunc != nil {
				clock.NowFunc = tt.nowFunc
			}

			msg, err := processRequest(tt.input, tt.client, zap.NewNop(), &clock, cfg)
			if tt.expectedErr != "" {
				require.EqualError(t, err, tt.expectedErr)
				assert.Nil(t, msg)
			} else {
				require.NoError(t, err)
				if tt.validate != nil {
					tt.validate(t, msg)
				}
			}
		})
	}
}
