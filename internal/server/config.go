package server

import (
	"time"
)

type HashcashConfig struct {
	ZerosCount    int
	Duration      time.Duration
	MaxIterations int
}
