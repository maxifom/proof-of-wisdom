package logging

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func Init(service string, devMode bool) (*zap.Logger, error) {
	initialFields := map[string]interface{}{
		"service": service,
		"system":  "proof-of-wisdom",
	}

	var cfg zap.Config
	if devMode {
		cfg = zap.NewDevelopmentConfig()
		cfg.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	} else {
		cfg = zap.NewProductionConfig()
		cfg.EncoderConfig.TimeKey = "timestamp"
		cfg.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
		cfg.EncoderConfig.EncodeDuration = zapcore.MillisDurationEncoder
	}
	cfg.InitialFields = initialFields

	logger, err := cfg.Build(zap.AddStacktrace(zap.ErrorLevel))
	if err != nil {
		return nil, err
	}

	return logger, nil
}
