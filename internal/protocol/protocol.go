package protocol

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

// Header of TCP-message in protocol, designates type of message sent.
const (
	RequestChallenge  = iota + 1 // Client requests challenge from server
	ResponseChallenge            // Server responds with challenge for client
	RequestResource              // Client requests resources with solved challenge from server
	ResponseResource             // Server responds with resource for client
)

// Message - message struct for server and client.
type Message struct {
	Header  int    // integer number to indicate type of transmitted payload
	Payload []byte // optional string, containing JSON data or quote
}

// String - converts message to string form to send over TCP connection.
func (m *Message) String() string {
	return fmt.Sprintf("%d|%s", m.Header, string(m.Payload))
}

// ParseMessage - parses Message from given string.
func ParseMessage(str string) (*Message, error) {
	str = strings.TrimSpace(str)

	parts := strings.SplitN(str, "|", 2)

	msgType, err := strconv.Atoi(parts[0])
	if err != nil {
		return nil, fmt.Errorf("cannot parse header: %w", err)
	}

	return &Message{Header: msgType, Payload: []byte(parts[1])}, nil
}

// ReadMessage reads and parses a Message from a given reader.
func ReadMessage(r *bufio.Reader) (*Message, error) {
	rawMsg, err := r.ReadString('\n')
	if err != nil {
		return nil, fmt.Errorf("error reading message: %w", err)
	}

	msg, err := ParseMessage(rawMsg)
	if err != nil {
		return nil, fmt.Errorf("error parsing message: %w", err)
	}

	return msg, nil
}

// WriteMessage writes a given Message to the provided writer.
func WriteMessage(w io.Writer, msg Message) error {
	_, err := fmt.Fprintf(w, "%s\n", msg.String())
	if err != nil {
		return fmt.Errorf("error writing message: %w", err)
	}
	return nil
}
