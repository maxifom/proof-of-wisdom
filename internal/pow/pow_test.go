package pow

import (
	"encoding/base64"
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_sha256Hash(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		data string
		want string
	}{
		{
			name: "Jason Statham",
			data: "Jason Statham",
			want: "c74833df570e0b81661c242d41548795969ead2f88a886b2cbae45ee26cdbc5c",
		},
		{
			name: "theman",
			data: "theman",
			want: "992345f21b57d68f497b9c5dbf837e060eaf2d8a4894f3f98c0b64de2b13006d",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			assert.Equalf(t, tt.want, sha256Hash(tt.data), "sha256Hash(%v)", tt.data)
		})
	}
}

func TestIsHashCorrect(t *testing.T) {
	t.Parallel()

	type args struct {
		hash       string
		zerosCount int
	}

	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "4 zeros correct",
			args: args{
				hash:       "000033df570e0b81661c242d41548795969ead2f88a886b2cbae45ee26cdbc5c",
				zerosCount: 4,
			},
			want: true,
		},
		{
			name: "5 zeros incorrect",
			args: args{
				hash:       "0000",
				zerosCount: 5,
			},
			want: false,
		},
		{
			name: "4 zeros incorrect",
			args: args{
				hash:       "000345f21b57d68f497b9c5dbf837e060eaf2d8a4894f3f98c0b64de2b13006d",
				zerosCount: 4,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			assert.Equalf(t, tt.want, IsHashCorrect(tt.args.hash, tt.args.zerosCount), "IsHashCorrect(%v, %v)", tt.args.hash, tt.args.zerosCount)
		})
	}
}

func TestComputeHashcash(t *testing.T) {
	t.Parallel()
	type fields struct {
		Version    int
		ZerosCount int
		Date       string
		Resource   string
		Rand       string
		Counter    int
	}
	type args struct {
		maxIterations int
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantCounter int
		wantErr     assert.ErrorAssertionFunc
	}{
		{
			name: "4 zeros",
			fields: fields{
				Version:    1,
				ZerosCount: 4,
				Date:       "2027-02-11T01:28:00Z",
				Resource:   "kekw",
				Rand:       base64.StdEncoding.EncodeToString([]byte(strconv.FormatInt(123459, 10))),
				Counter:    0,
			},
			args: args{
				maxIterations: 500000,
			},
			wantCounter: 87291,
			wantErr:     nil,
		},
		{
			name: "5 zeros",
			fields: fields{
				Version:    1,
				ZerosCount: 5,
				Date:       "2024-02-11T01:28:00Z",
				Resource:   "kekw",
				Rand:       base64.StdEncoding.EncodeToString([]byte(strconv.FormatInt(123460, 10))),
				Counter:    0,
			},
			args: args{
				maxIterations: 20000000,
			},
			wantCounter: 2457328,
			wantErr:     nil,
		},
		{
			name: "Impossible challenge",
			fields: fields{
				Version:    1,
				ZerosCount: 10,
				Date:       "2011-02-11T01:28:00Z",
				Resource:   "kekw",
				Rand:       base64.StdEncoding.EncodeToString([]byte(strconv.FormatInt(123460, 10))),
				Counter:    0,
			},
			args: args{
				maxIterations: 10,
			},
			wantCounter: 11,
			wantErr: func(t assert.TestingT, err error, _ ...interface{}) bool {
				return assert.Equal(t, "max iterations exceeded", err.Error())
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			date, _ := time.Parse(time.RFC3339, tt.fields.Date)
			h := HashcashData{
				Version:    tt.fields.Version,
				ZerosCount: tt.fields.ZerosCount,
				Date:       date.Unix(),
				Resource:   tt.fields.Resource,
				Rand:       tt.fields.Rand,
				Counter:    tt.fields.Counter,
			}
			got, err := h.ComputeHashcash(tt.args.maxIterations)
			if tt.wantErr != nil {
				tt.wantErr(t, err, fmt.Sprintf("ComputeHashcash(%v)", tt.args.maxIterations))
			}
			assert.Equalf(t, tt.wantCounter, got.Counter, "ComputeHashcash(%v)", tt.args.maxIterations)
		})
	}
}
