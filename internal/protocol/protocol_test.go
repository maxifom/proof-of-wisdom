package protocol

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParseMessage(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name        string
		input       string
		expectedErr string
		expectedMsg *Message
	}{
		{
			name:        "Empty message",
			input:       "",
			expectedErr: "cannot parse header: strconv.Atoi: parsing \"\": invalid syntax",
			expectedMsg: nil,
		},
		{
			name:        "Invalid message",
			input:       "||",
			expectedErr: "cannot parse header: strconv.Atoi: parsing \"\": invalid syntax",
			expectedMsg: nil,
		},
		{
			name:        "Message with non-digit header",
			input:       "test|payload",
			expectedErr: "cannot parse header: strconv.Atoi: parsing \"test\": invalid syntax",
			expectedMsg: nil,
		},
		{
			name:        "Message with only header",
			input:       "1|",
			expectedErr: "",
			expectedMsg: &Message{Header: 1, Payload: []byte{}},
		},
		{
			name:        "Message with header and payload",
			input:       "1|payload",
			expectedErr: "",
			expectedMsg: &Message{Header: 1, Payload: []byte("payload")},
		},
		{
			name:        "Message with header and payload, that contains pipe separator",
			input:       "1|p|a|y|l|o|ad",
			expectedErr: "",
			expectedMsg: &Message{Header: 1, Payload: []byte("p|a|y|l|o|ad")},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			msg, err := ParseMessage(tt.input)
			if tt.expectedErr != "" {
				require.EqualError(t, err, tt.expectedErr)
				assert.Nil(t, msg)
			} else {
				require.NoError(t, err)
				require.NotNil(t, msg)
				assert.Equal(t, tt.expectedMsg.Header, msg.Header)
				assert.Equal(t, tt.expectedMsg.Payload, msg.Payload)
			}
		})
	}
}
