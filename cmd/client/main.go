package main

import (
	"context"
	"fmt"
	"os/signal"
	"syscall"

	"github.com/kelseyhightower/envconfig"
	"go.uber.org/zap"

	"proof-of-wisdom/internal/client"
	"proof-of-wisdom/internal/logging"
)

type Config struct {
	ServerAddr      string `default:"127.0.0.1:3333" envconfig:"SERVER_ADDR"`
	DevelopmentMode bool   `default:"true"           envconfig:"DEVELOPMENT_MODE"`

	HashcashMaxIterations int `default:"1000000" envconfig:"HASHCASH_MAX_ITERATIONS"`
}

func main() {
	var conf Config
	err := envconfig.Process("", &conf)
	if err != nil {
		panic(fmt.Errorf("failed to process env: %w", err))
	}

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGTERM, syscall.SIGINT)
	defer stop()

	logger, err := logging.Init("client", conf.DevelopmentMode)
	if err != nil {
		panic(fmt.Errorf("failed to init logger: %w", err))
	}

	err = client.Run(ctx, conf.ServerAddr, logger, conf.HashcashMaxIterations)
	if err != nil {
		logger.Panic("failed to run client", zap.Error(err))
	}
}
