package pow

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
)

const zeroByte = '0'

// HashcashData - struct with fields of Hashcash.
type HashcashData struct {
	Version    int    `json:"version"`
	ZerosCount int    `json:"zeros_count"`
	Date       int64  `json:"date"`
	Resource   string `json:"resource"`
	Rand       string `json:"rand"`
	Counter    int    `json:"counter"`
}

// String - stringifies hashcash for next sending it on TCP.
func (h HashcashData) String() string {
	return fmt.Sprintf("%d:%d:%d:%s::%s:%d", h.Version, h.ZerosCount, h.Date, h.Resource, h.Rand, h.Counter)
}

// sha256Hash - calculates sha256 hash from given string.
func sha256Hash(data string) string {
	hash := sha256.Sum256([]byte(data))
	return hex.EncodeToString(hash[:])
}

// IsHashCorrect - checks that hash has leading <zerosCount> zeros.
func IsHashCorrect(hash string, zerosCount int) bool {
	if zerosCount > len(hash) {
		return false
	}
	for i := 0; i < zerosCount; i++ {
		if hash[i] != zeroByte {
			return false
		}
	}
	return true
}

// ComputeHashcash - calculates correct hashcash by bruteforce
// until the resulting hash satisfies the condition of IsHashCorrect.
func (h HashcashData) ComputeHashcash(maxIterations int) (HashcashData, error) {
	if maxIterations <= 0 {
		return h, fmt.Errorf("max iterations should be more than 0")
	}

	for h.Counter <= maxIterations {
		header := h.String()
		hash := sha256Hash(header)
		if IsHashCorrect(hash, h.ZerosCount) {
			return h, nil
		}

		h.Counter++
	}

	return h, fmt.Errorf("max iterations exceeded")
}
