FROM golang:1.22.5-alpine3.20 AS builder

WORKDIR /goman/

COPY go.mod go.sum ./

RUN go mod download -x

COPY . .
RUN GOBIN=/goman/apps CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go install ./cmd/client/

FROM scratch

COPY --from=builder /goman/apps/ /

ENTRYPOINT ["/client"]
