package server

import (
	"bufio"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math/rand"
	"net"
	"strconv"
	"time"

	"go.uber.org/zap"

	"proof-of-wisdom/internal/pow"
	"proof-of-wisdom/internal/protocol"
)

// Quotes - array of Jason Statham's quotes to respond on client's request.
var Quotes = []string{
	"I've come from nowhere, and I'm not shy to go back.",
	"Do 40 hard minutes, not an hour and a half of nonsense.",
	"Looking good and feeling good go hand in hand. If you have a healthy lifestyle, your diet and nutrition are set, and you're working out, you're going to feel good.",
	"People take chances every now and then, and you don't want to disappoint them.",
	"Do I look like a man who came halfway across Europe to die on a bridge?",
	"I should never go with rumors because they’re often not correct.",
	"I forbid you to not like this project",
}

// Run - starts server TCP listener on provided address and handle new connections.
func Run(ctx context.Context, address string, logger *zap.Logger, clock Clock, hashcashConfig HashcashConfig) error {
	lc := net.ListenConfig{}
	listener, err := lc.Listen(ctx, "tcp", address)
	if err != nil {
		return fmt.Errorf("failed to start listener on %s: %w", address, err)
	}
	defer listener.Close()

	logger.Info("Server started", zap.String("address", listener.Addr().String()))
	for {
		connChan := make(chan acceptConnectionResult)
		go acceptConnection(listener, connChan)

		select {
		case <-ctx.Done():
			return ctx.Err()
		case connRes := <-connChan:
			if connRes.err != nil {
				return connRes.err
			}
			go handleConnection(ctx, connRes.conn, logger, clock, hashcashConfig)
		}
	}
}

func acceptConnection(listener net.Listener, connChan chan acceptConnectionResult) {
	conn, err := listener.Accept()
	connChan <- acceptConnectionResult{conn: conn, err: err}
}

type acceptConnectionResult struct {
	conn net.Conn
	err  error
}

func handleConnection(ctx context.Context, conn net.Conn,
	logger *zap.Logger, clock Clock, hashcashConfig HashcashConfig) {
	defer conn.Close()
	connLogger := logger.With(zap.String("address", conn.RemoteAddr().String()))
	reader := bufio.NewReader(conn)
	done := make(chan error)

	go processMessages(reader, conn, connLogger, clock, hashcashConfig, done)

	select {
	case err := <-done:
		if err != nil {
			connLogger.Error("failed to handle connection", zap.Error(err))
		}
	case <-ctx.Done():
		connLogger.Error("connection closed by context", zap.Error(ctx.Err()))
	}
}

func processMessages(reader *bufio.Reader, conn net.Conn,
	logger *zap.Logger, clock Clock, hashcashConfig HashcashConfig, done chan error) {
	for {
		msg, err := protocol.ReadMessage(reader)
		if err != nil {
			done <- fmt.Errorf("failed to read message from connection: %w", err)
			return
		}

		responseMessage, err := processRequest(msg, conn.RemoteAddr().String(), logger, clock, hashcashConfig)
		if err != nil {
			done <- fmt.Errorf("failed to process request: %w", err)
			return
		}

		if responseMessage != nil {
			err = protocol.WriteMessage(conn, *responseMessage)
			if err != nil {
				done <- fmt.Errorf("failed to send response message: %w", err)
				return
			}
		}
	}
}

// processRequest - process request from client.
func processRequest(msg *protocol.Message, clientInfo string,
	logger *zap.Logger, clock Clock, hashcashConfig HashcashConfig) (*protocol.Message, error) {
	switch msg.Header {
	case protocol.RequestChallenge:
		return handleRequestChallenge(clientInfo, logger, clock, hashcashConfig)
	case protocol.RequestResource:
		return handleRequestResource(msg, clientInfo, logger, clock, hashcashConfig)
	default:
		return nil, fmt.Errorf("unknown header: %d", msg.Header)
	}
}

func handleRequestChallenge(clientInfo string,
	logger *zap.Logger, clock Clock, hashcashConfig HashcashConfig) (*protocol.Message, error) {
	logger.Debug("Client requests challenge")

	date := clock.Now()
	randValue := rand.Intn(1e5)

	hashcash := pow.HashcashData{
		Version:    1,
		ZerosCount: hashcashConfig.ZerosCount,
		Date:       date.Unix(),
		Resource:   clientInfo,
		Rand:       base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(randValue))),
		Counter:    0,
	}

	hashcashMarshaled, err := json.Marshal(hashcash)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal hashcash: %w", err)
	}

	return &protocol.Message{
		Header:  protocol.ResponseChallenge,
		Payload: hashcashMarshaled,
	}, nil
}

func handleRequestResource(msg *protocol.Message, clientInfo string,
	logger *zap.Logger, clock Clock, hashcashConfig HashcashConfig) (*protocol.Message, error) {
	logger.Debug("Client requests resource")

	var hashcash pow.HashcashData
	err := json.Unmarshal(msg.Payload, &hashcash)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal hashcash: %w", err)
	}

	if hashcash.Resource != clientInfo {
		return nil, fmt.Errorf("invalid hashcash resource %s != %s", hashcash.Resource, clientInfo)
	}

	if isChallengeExpired(hashcash, clock, hashcashConfig) {
		return nil, fmt.Errorf("challenge expired %s > %s", clock.Now().Sub(time.Unix(hashcash.Date, 0)), hashcashConfig.Duration)
	}

	if _, err := hashcash.ComputeHashcash(hashcash.Counter); err != nil {
		return nil, fmt.Errorf("invalid hashcash: %w", err)
	}

	logger.Debug("Client successfully computed hashcash")

	return &protocol.Message{
		Header:  protocol.ResponseResource,
		Payload: []byte(Quotes[rand.Intn(len(Quotes))]),
	}, nil
}

func isChallengeExpired(hashcash pow.HashcashData, clock Clock, hashcashConfig HashcashConfig) bool {
	since := clock.Now().Sub(time.Unix(hashcash.Date, 0))
	return since > hashcashConfig.Duration
}
