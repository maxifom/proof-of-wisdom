package client

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"

	"proof-of-wisdom/internal/pow"
	"proof-of-wisdom/internal/protocol"
)

// MockConnection - mocks tcp connection by two interfaces (reader and writer) and funcs.
type MockConnection struct {
	ReadFunc  func([]byte) (int, error)
	WriteFunc func([]byte) (int, error)
}

func (m MockConnection) Read(p []byte) (n int, err error) {
	return m.ReadFunc(p)
}

func (m MockConnection) Write(p []byte) (n int, err error) {
	return m.WriteFunc(p)
}

func TestHandleConnection(t *testing.T) {
	t.Parallel()

	ctx := context.Background()
	maxIterations := 1000000

	tests := []struct {
		name         string
		mockConn     MockConnection
		expectedErr  string
		expectedResp string
	}{
		{
			name: "Write error",
			mockConn: MockConnection{
				WriteFunc: func([]byte) (int, error) {
					return 0, fmt.Errorf("test write error")
				},
			},
			expectedErr:  "failed to send request challenge message: error writing message: test write error",
			expectedResp: "",
		},
		{
			name: "Read error",
			mockConn: MockConnection{
				WriteFunc: func([]byte) (int, error) {
					return 0, nil
				},
				ReadFunc: func([]byte) (int, error) {
					return 0, fmt.Errorf("test read error")
				},
			},
			expectedErr:  "failed to read request challenge response: error reading message: test read error",
			expectedResp: "",
		},
		{
			name: "Read response in bad format",
			mockConn: MockConnection{
				WriteFunc: func([]byte) (int, error) {
					return 0, nil
				},
				ReadFunc: func(p []byte) (int, error) {
					return fillByteSliceFromString("||\n", p), nil
				},
			},
			expectedErr:  "failed to read request challenge response: error parsing message: cannot parse header: strconv.Atoi: parsing \"\": invalid syntax",
			expectedResp: "",
		},
		{
			name: "Read response with hashcash in bad format",
			mockConn: MockConnection{
				WriteFunc: func([]byte) (int, error) {
					return 0, nil
				},
				ReadFunc: func(p []byte) (int, error) {
					return fillByteSliceFromString(fmt.Sprintf("%d|{wrong_json}\n", protocol.ResponseChallenge), p), nil
				},
			},
			expectedErr:  "failed to unmarshal hashcash challenge: invalid character 'w' looking for beginning of object key string",
			expectedResp: "",
		},
		{
			name: "Success",
			mockConn: func() MockConnection {
				date := time.Date(2019, 1, 6, 7, 9, 8, 555, time.UTC)
				hashcash := pow.HashcashData{
					Version:    1,
					ZerosCount: 1,
					Date:       date.Unix(),
					Resource:   "client1",
					Rand:       base64.StdEncoding.EncodeToString([]byte(strconv.FormatInt(123460, 10))),
					Counter:    999,
				}

				readAttempt := 0
				writeAttempt := 0

				return MockConnection{
					WriteFunc: func(p []byte) (int, error) {
						if writeAttempt == 0 {
							writeAttempt++
							assert.Equal(t, "1|\n", string(p))
						} else {
							msg, err := protocol.ParseMessage(string(p))
							require.NoError(t, err)
							var writtenHashcash pow.HashcashData
							err = json.Unmarshal(msg.Payload, &writtenHashcash)
							require.NoError(t, err)

							assert.Equal(t, 1004, writtenHashcash.Counter)
							_, err = writtenHashcash.ComputeHashcash(1005)
							assert.NoError(t, err)
						}

						return 0, nil
					},
					ReadFunc: func(p []byte) (int, error) {
						if readAttempt == 0 {
							marshaled, err := json.Marshal(hashcash)
							require.NoError(t, err)
							readAttempt++

							return fillByteSliceFromString(fmt.Sprintf("%d|%s\n", protocol.ResponseChallenge, string(marshaled)), p), nil
						}

						return fillByteSliceFromString(fmt.Sprintf("%d|test quote\n", protocol.ResponseChallenge), p), nil
					},
				}
			}(),
			expectedErr:  "",
			expectedResp: "test quote",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			response, err := handleConnection(ctx, tt.mockConn, zap.NewNop(), maxIterations)
			if tt.expectedErr != "" {
				require.EqualError(t, err, tt.expectedErr)
				assert.Empty(t, response)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tt.expectedResp, response)
			}
		})
	}
}

// fillByteSliceFromString - fills byte slice from given string for mocking reader.
func fillByteSliceFromString(str string, p []byte) int {
	dataBytes := []byte(str)
	counter := 0

	for i := range dataBytes {
		p[i] = dataBytes[i]
		counter++
		if counter >= len(p) {
			break
		}
	}

	return counter
}
