package main

import (
	"context"
	"fmt"
	"os/signal"
	"syscall"
	"time"

	"github.com/kelseyhightower/envconfig"
	"go.uber.org/zap"

	"proof-of-wisdom/internal/logging"
	"proof-of-wisdom/internal/server"
)

type Config struct {
	ListenAddr      string `default:"127.0.0.1:3333" envconfig:"LISTEN_ADDR"`
	DevelopmentMode bool   `default:"true"           envconfig:"DEVELOPMENT_MODE"`

	HashcashZerosCount    int           `default:"3"       envconfig:"HASHCASH_ZEROS_COUNT"`
	HashcashDuration      time.Duration `default:"3s"      envconfig:"HASHCASH_DURATION"`
	HashcashMaxIterations int           `default:"1000000" envconfig:"HASHCASH_MAX_ITERATIONS"`
}

func main() {
	var conf Config
	err := envconfig.Process("", &conf)
	if err != nil {
		panic(fmt.Errorf("failed to process env: %w", err))
	}

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGTERM, syscall.SIGINT)
	defer stop()

	logger, err := logging.Init("client", conf.DevelopmentMode)
	if err != nil {
		panic(fmt.Errorf("failed to init logger: %w", err))
	}

	hashcashConfig := server.HashcashConfig{
		ZerosCount:    conf.HashcashZerosCount,
		Duration:      conf.HashcashDuration,
		MaxIterations: conf.HashcashMaxIterations,
	}

	err = server.Run(ctx, conf.ListenAddr, logger, server.SystemClock{}, hashcashConfig)
	if err != nil {
		logger.Panic("failed to run server", zap.Error(err))
	}
}
