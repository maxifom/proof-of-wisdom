# Proof of Wisdom (by Jason Statham)

## 1. Task

Design and implement "Word of Wisdom" tcp server.

* TCP server should be protected from DDOS attacks with the Proof of Work (https://en.wikipedia.org/wiki/Proof_of_work),
  the challenge-response protocol should be used.
* The choice of the POW algorithm should be explained.
* After Proof Of Work verification, server should send one of the quotes from "word of wisdom" book or any other
  collection of the quotes.
* Docker file should be provided both for the server and for the client that solves the POW challenge

## 2. Getting started

### 2.1 Requirements

+ Go 1.22 to run tests, start server or client locally
+ or Docker to run project using docker-compose

### 2.2 Start server and client using docker-compose:

```
make up
```

### 2.3 Start only server (locally):

```
make start-server
```

### 2.4 Start only client (locally):

```
make start-client
```

### 2.5 Launch tests:

```
make test
```

## 3. Protocol definition

In this project I used TCP-based protocol. Each transmitted message divided by \n and consists of two parts divided by
symbol |:

+ header - integer number to indicate type of transmitted payload
+ payload - optional string, containing JSON data or quote

### 3.1 Message types

Depending on the header following message types are supported and considered valid:

+ 1 - RequestChallenge - Client requests challenge from server
+ 2 - ResponseChallenge - Server responds with challenge for client
+ 3 - RequestResource - Client requests resources with solved challenge from server
+ 4 - ResponseResource - Server responds with resource for client

### 3.2 Examples

+ 1 RequestChallenge: ```1|```
+ 2
  ResponseChallenge: ```2|{"Version":1,"ZerosCount":3,"Date":1654461429,"Resource":"maxifom","Rand":"bWF4aWZvbQ==","Counter":0}```
+ 3
  RequestResource: ```3|{"Version":1,"ZerosCount":3,"Date":1654461429,"Resource":"maxifom","Rand":"bWF4aWZvbQ==","Counter":9999}```
+ 4 ResponseResource ```4|I forbid you to not like this project```

## 4. Proof of Work

A proof-of-work (PoW) system allows a verifier to check with negligible effort that a prover has expended a some amount
of computational effort.

The idea of Proof of Work for DDOS protection is that the client, which wants to get some resource from server,
should first solve challenge provided by server to prove that he has done some amount of work and can access the
resource.

### 4.1 Algorithm selection

There is some different algorithms of Proof Work.
I compared next three algorithms as more understandable and having most extensive documentation:

+ Cuckoo cycle
+ Hashcash
+ Guided tour puzzle

After careful consideration, Hashcash was chosen as an algorithm for this project. The other 2 algorithms have following
disadvantages:

+ Cuckoo Cycle requires loads of memory and processing time on client side to calculate 
+ Guided tour puzzle requires client to regularly request server about next parts of guide, that floods the network and
  complicates logic of protocol.

Hashcash, instead has next advantages:

+ Easy to implement in Golang
+ Fast and easy validation on server side
+ Can be scaled to increased difficulty as hardware becomes faster and faster

But Hashcash also has some disadvantages:

1. Bad actors can pre-compute challenges in advance before DDOS-attack and overflow system with solved challenges.
   In production environment it could be solved by storing Rand parameter to some persistent storage and check its
   existence during verification
2. Compute time depends on power of client's machine.
   Clients with low computational power may sometimes not solve the challenge provided, and bad actors with powerful
   computers could try to attempt a DDOS-attack.
   This could be solved by dynamically changing complexity of provided challenge depending on current load. During "
   good" times almost all clients, even weak ones will be able to solve, but during DDOS-attacks there
   will be some difficulties for real and bad users of the system.

## 5. Project structure

Existing directories:

+ cmd/client - Entypoint for client
+ cmd/server - Entrypoint for server
+ internal/client - Client logic
+ internal/server - Server logic
+ internal/logging - Logging utility
+ internal/pow - PoW algorithm implemenation (Hashcash)
+ internal/protocol - TCP protocol specification and logic

## 6. Further improvements

I can see a few ways to improve this project, if it's going to be deployed in production environment:

+ Use database for storing and retrieving quotes
+ Simulate DDOS attacks by spawning more clients to detect bottlenecks and problems before deploying to production
+ Add dynamic management of Hashcash complexity based on server load to protect against any DDOS attacks and mitigate
  issue with slow client devices
