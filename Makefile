test:
	go clean --testcache
	go test ./...

start-server:
	go run cmd/server/main.go

start-client:
	go run cmd/client/main.go

up:
	docker compose up --abort-on-container-exit --force-recreate --build server client
